$(function () {
    // Lightbox 
    $(document).on('click', '[data-toggle="lightbox"]', function (e) {
        e.preventDefault();
        $(this).ekkoLightbox()
    })

    //Copyright vandaag
    let aMaanden = ["januari", "februari", "maart", "april", "mei", "juni", "julli", "augustus", "september", "oktober", "november", "december"]
    let datum = new Date;

    let maand = aMaanden[datum.getMonth()];
    let dag = datum.getDate();

    let vandaag = dag + ' ' + maand + ' ' + datum.getFullYear();

    $("#datum").html(vandaag);
})